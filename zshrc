# TODO: Document what this file is.
# (See "STARTUP/SHUTDOWN FILES" in zsh(1).)

# The variable PS1 controls the shell prompt (the '%' that's printed before the
# command line).
#
# %m expands to the hostname.
# %~ expands to the current directory.
#
# TODO man page reference
PS1="%m:%~%# "

# Save command history
#
# TODO explain the magic variables and the syntax
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=${XDG_CACHE_HOME:-"${HOME}/.cache"}/zsh_history

# Load the completion system.
#
# "autoload" serves a similar purpose as "include", "import", "use", or
# "require" in other languages.
#
# TODO man page reference
autoload -Uz compinit && compinit

# Tell the completion system to show different kinds of completions differently.
# For example, after "ssh <TAB>" you'll see that usernames and hostnames are
# listed separately.
#
# TODO man page reference
zstyle ':completion:*' group-name ''
zstyle ':completion:*' format '>>> %d'
